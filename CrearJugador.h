/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   CrearJugador.h
 * Author: luigitercero
 *
 * Created on 15 de junio de 2018, 05:39 PM
 */

#ifndef CREARJUGADOR_H
#define CREARJUGADOR_H

#include "CrearMemoria.h"

class CrearJugador {
public:
   
    int getJugador();
    CrearJugador(int idMemoria);
    CrearJugador(const CrearJugador& orig);
    virtual ~CrearJugador();
        CrearMemoria player;
         
       
private:
    int jugador;

};

#endif /* CREARJUGADOR_H */

