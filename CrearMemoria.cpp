/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   CrearJuego.cpp
 * Author: luigitercero
 * 
 * Created on 15 de junio de 2018, 09:05 AM
 */

#include "CrearMemoria.h"

CrearMemoria::CrearMemoria(int clave) {
    key = clave;
    crearClave(clave);
    crearMemoria();
    usarMemoria();
    Memoria[70]=Memoria[70]+1;
 
}

CrearMemoria::CrearMemoria(){
}
CrearMemoria::CrearMemoria(const CrearMemoria& orig) {


}

CrearMemoria::~CrearMemoria() {
}

int CrearMemoria::crearClave(int clave) {
    Clave = ftok("/bin/ls", clave);
    if (Clave == -1) {
        std::cout << "No consigo clave para memoria compartida" << std::endl;
        exit(0);
    }
    return 1;
}

int CrearMemoria::crearMemoria() {
    Id_Memoria = shmget(Clave, sizeof (int)*100, 0777 | IPC_CREAT);
    if (Id_Memoria == -1) {
        std::cout << "No consigo Id para memoria compartida" << std::endl;
        exit(0);
    }
    return 1;
}

int CrearMemoria::usarMemoria() {
    Memoria = (int *) shmat(Id_Memoria, (char *) 0, 0);
    if (Memoria == NULL) {
        std::cout << "No consigo memoria compartida" << std::endl;
        exit(0);
    }
    return 1;
}

int CrearMemoria::eliminarMemoria() {
    shmdt((char *) Memoria);
    shmctl(Id_Memoria, IPC_RMID, (struct shmid_ds *) NULL);
    return 1;
}

int * CrearMemoria::getMemoria() {
    return Memoria;
}



