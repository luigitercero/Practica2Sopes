/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Juego.h
 * Author: luigitercero
 *
 * Created on 15 de junio de 2018, 10:04 PM
 */

#ifndef JUEGO_H
#define JUEGO_H
#include <iostream>
class Juego {
public:
    Juego(int jugador, int *Memoria);
    Juego();
    Juego(const Juego& orig);
    virtual ~Juego();
    int igresePalabra(char* pal);
    int MenuPrincipal();
    int MenuSalida();
    int mostrarTablero();
    int getSoyJugador();
    int cargarOtraPalabra();
    int menuSito(int opcion);
    int isGanador ();
private:
    int adivina[16];
    std::string  miPalabra;
    std::string  otrapalabra;
    int listaPrincipal();
    char *ingresePalabra();
    int randPalabraas();
    int soyJugador = 0;
    int *miTablero;
};

#endif /* JUEGO_H */

