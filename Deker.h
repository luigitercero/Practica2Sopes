/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Deker.h
 * Author: luigitercero
 *
 * Created on 15 de junio de 2018, 04:44 PM
 */

#ifndef DEKER_H
#define DEKER_H

class Deker {
public:
    int * turno;
    Deker();
    Deker(const Deker& orig);
    Deker(int *t);
    virtual ~Deker();
    int hacerTarea(int  jugardor,int (*tareaInicial)(),int (*seccionCritica)(),int (*tareaFinal)());
    int callBack(void (*p)());
private:
    int idProceso;
};

#endif /* DEKER_H */

