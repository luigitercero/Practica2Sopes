/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   CrearJuego.h
 * Author: luigitercero
 *
 * Created on 15 de junio de 2018, 09:05 AM
 */

#ifndef CREARJUEGO_H
#define CREARJUEGO_H
#include <sys/shm.h>
#include <iostream>
#include <unistd.h>

class CrearMemoria {
public:
    CrearMemoria();
    CrearMemoria(int a);
    CrearMemoria(const CrearMemoria& orig);
    virtual ~CrearMemoria();
    int *getMemoria();
    int eliminarMemoria();
    int key;

private:

    key_t Clave;
    int Id_Memoria;
    int *Memoria = NULL;
    int i, j;
    int respuestas[16];
    int crearClave(int a);
    int crearMemoria();
    int usarMemoria();
   
   

};

#endif /* CREARJUEGO_H */

