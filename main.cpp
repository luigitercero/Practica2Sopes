/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.cpp
 * Author: luigitercero
 *
 * Created on 15 de junio de 2018, 08:50 AM
 */

#include <cstdlib>

#include "CrearJugador.h"
#include "Juego.h"
#include "Deker.h"

using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {
    std::cout << "iniciando Juego"  << std::endl;
    CrearJugador jueg(33); //sentencia incial
    
    Juego juego(jueg.getJugador(), jueg.player.getMemoria());//sentencia inicial
    if (juego.getSoyJugador() == 1) {
        jueg.player.getMemoria()[0] = 2;
        std::cout << "Esperando al segundo Jugador" << jueg.player.getMemoria()[0] << std::endl;
        while (jueg.player.getMemoria()[0] == 2) {}
        juego.cargarOtraPalabra(); //region critica
    } else {
        std::cout << "Esperando al otro Jugador" << std::endl;
        while (jueg.player.getMemoria()[0] == 1) {}
        juego.cargarOtraPalabra();//region critica
        jueg.player.getMemoria()[0] = 1;
    }
    //asignando turnos
    int turno = 0;
    int salida = 0;
    if (juego.getSoyJugador() == 1) {
        turno = 2;
    } else {
        turno = 1;
    }
    while (true) {
        juego.mostrarTablero();//sentencias iniciles
        std::cout << "Esperando al otro Jugador" << std::endl;
        while (jueg.player.getMemoria()[0] == turno) {
        }

        if (juego.isGanador() == -1) {//region critica
            salida = 3;
            break;
        }
        salida = juego.MenuPrincipal(); //region critica
        jueg.player.getMemoria()[0] = turno;
        if (salida == -1 || salida == 3) {break;}//taras finales
    }
    if (salida == 3) {
        jueg.player.eliminarMemoria();// fin de algoritmo
    }


    return 0;
}

