/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Juego.cpp
 * Author: luigitercero
 * 
 * Created on 15 de junio de 2018, 10:04 PM
 */



#include "Juego.h"

Juego::Juego() {

}

Juego::Juego(const Juego& orig) {
}

Juego::~Juego() {
}

Juego::Juego(int jugador, int *Memoria) {
    soyJugador = jugador;
    miTablero = Memoria;
    for (int x = 0; x < 16; x++) {
        adivina[x] = 0;
    }
    randPalabraas();

}

int Juego::randPalabraas() {
    std::string pal;

    int posicion = (soyJugador - 1) * 16 + 1; //cada jugador tiene un espacio de 16 en un tablero de 4*4
    int espPalabra = (32 + 1) + (soyJugador - 1) * 6;
    std::cout << "ingrese la palabra que va jugar\n";
    std::cin >> pal;

    int arreglo [pal.size()];
    int pos;
    int a;
    for (a = 0; a < pal.size(); a++) {

        pos = rand() % 16 + 1;
        for (int x = 0; x < pal.size(); x++) {
            if (arreglo[a] == pos) {
                pos = rand() % 16 + 1;
                x = 0;

            }
        }
        arreglo[a] = pos;
        miTablero[pos + posicion ] = pal[a];
        miTablero[espPalabra + a] = pal[a];
        std::cout << "posicion  " << espPalabra + a;
        std::cout << " Letra guardada " << miTablero[pos + posicion ];
        std::cout << "\n";
    }
    miTablero[espPalabra + a] = 0;
    miPalabra = pal;

    return 1;
}

int Juego::MenuPrincipal() {
    int opcion = listaPrincipal();
    while (true) {

        if (opcion > 0 && opcion < 4) {
            break;
        }
        std::cout << "opcion no aceptada" << std::endl;
    }
    int salida = menuSito(opcion);


    return salida;
}

int Juego::isGanador() {

    if (miTablero[85] != 0) {
        if (miTablero[85] != soyJugador) {
            std::cout << "Perdiste " << std::endl;
            return -1;
        } else {
            std::cout << "Ganaste :)" << std::endl;
            return -1;
        }
    }

    return 1;



}

int Juego::menuSito(int opcion) {
    int a = -1;
    std::string palabra;
    std::string nueva;
    switch (opcion) {
        case 1:
            while ((a <= -1) || (a >= 16)) {
                std::cout << "elija una casilla valida " << std::endl;
                std::cin>> a;
            }
            adivina[a] = 1;
            break;
        case 2:
            
            std::cout << "Escriba una palabra que sea correcta " << std::endl;
            std::cin>> palabra;
            if (!otrapalabra.compare(palabra)) {
                std::cout << "ganaste :'} ''" << std::endl;
                miTablero[85] = soyJugador;
                return -1;
            } else {
                std::cout << "fallaste " << std::endl;
            }
            return opcion;
        case 3:
            if (soyJugador == 1) {
                 std::cout << "perdiste " << std::endl;
                miTablero[85] = 2;
            } else {
                std::cout << "perdiste " << std::endl;
                miTablero[85] = 1;
            }
            return 3;

    }
    return opcion;
}

int Juego::listaPrincipal() {
    int opcion;

    std::cout << "1.) Buscar Letra" << std::endl;
    std::cout << "2.) Ingresar Palabra" << std::endl;
    std::cout << "3.) Redirse" << std::endl;
    std::cin >> opcion;

    return opcion;
}

int Juego::getSoyJugador() {
    return soyJugador;
}

int Juego::cargarOtraPalabra() {
    otrapalabra = "";
    char nueva[7];
    int ubicacionOtraPalabra;
    if (getSoyJugador() == 1) {
        ubicacionOtraPalabra = 32 + 1 + 6;
    } else {
        ubicacionOtraPalabra = 32 + 1;
    }
    std::cout << "se cargara la otra \n";
    for (int x = 0; x < 5; x++) {
        nueva[x] = 0;
        nueva[x] = miTablero [x + ubicacionOtraPalabra];
        std::cout << "ubicacion " << x + ubicacionOtraPalabra;
        std::cout << " letra " << miTablero [x + ubicacionOtraPalabra] << std::endl;
        ;
    }
    nueva[6] = 0;
    std::string gg(nueva);
    otrapalabra = gg;
    //std::cout<< "se cargo la palabra " << otrapalabra ;
    //std::cout<< "\n" ;
    return 1;
}

int Juego::mostrarTablero() {
    std::cout << "Mi palabra es " << miPalabra << std::endl;
    int ubicacionOtraPalabra;
    if (getSoyJugador() == 1) {
        ubicacionOtraPalabra = 16;
    } else {
        ubicacionOtraPalabra = 1;
    }
    int count = 0;
    int pos = 0;
    for (int x = 0; x < 16; x++) {
        if (count == 4) {
            std::cout << "\n";
            count = 0;
        }

        if (miTablero[ubicacionOtraPalabra + x] == 0) {
            if (adivina[x] == 0) {
                std::cout << "|| " << pos << " || ";

            } else {
                std::cout << "|| " << "x" << " || ";
            }
        } else {

            if (adivina[x] == 0) {
                std::cout << "|| " << pos << " || ";

            } else {
                char letra = miTablero[ubicacionOtraPalabra + x];
                std::cout << "|| " << letra << " || ";
            }

        }
        count++;
        pos++;
    }
    std::cout << "\n";
    return 1;
}

int Juego::MenuSalida() {
    return 3;
}